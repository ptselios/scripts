#!/bin/bash
# Author: Peter Tselios
# ########################################################################
# Copyright (c) 2019 Peter Tselios and ITCultus LTD
# 
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/.
#
########################################################################
# Script Name: dyndns.sh
###############################################################################
# Description: This script is used to update the DNS entries of the home server
#              It should be executed as a crontab or systemd.timer. 
#              The systemd.timer is more accurate and can be used in different
#              scenarios but requires 3 files (the .service, the .timer and the
#              actual script).
# INPUT PARAMETERS: None
#
# INPUT FILES: None
# OUTPUT FILES: /var/cache/dyndns
# 
###############################################################################
# Exit Codes:
# 1: Script no run with sudo or from root user
# 2: Wrong/corrupted cache file
# 3: dig not found
###############################################################################
# Version: 0.0.1

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ ! -x /usr/bin/dig ]; then
    echo "dig command either is missing or is not an executable"
    exit 3
fi

# Variables
#
# Initial speed test from my location indicated that the faster 
# resolver is google, then the opendns and finally akamai. 
# I chose to use akamai for no particular reason. If you want to use another 
# resolver, please comment the akamai variables and uncomment the variables of
# theresolver you want to use. 

##Google vars
#resolver='ns1.google.com'
#qtype=TXT
#qhost="o-o.myaddr.l.google.com"

#OpenDNS vars
#resolver='resolver1.opendns.com'
#qtype="ANY"
#qhost="myip.opendns.com"

#Akamai vars
resolver='ns1-1.akamaitech.net'
qtype="ANY"
qhost="whoami.akamai.net"
cnds=""

# DNS related variables 
domain="example.com"                        # your domain
type="A"                                    # Record type A, CNAME, MX, etc.
name="dhcp"                                 # name of record to update
ttl="3600"                                  # Time to Live min value 600
key=""    # key for godaddy developer API
secret=""              # secret for godaddy developer API
response=500

cdns=$(dig @${resolver} ${qtype} ${qhost} +short)

cachefile="/var/cache/dyndns/current"
currentip="0.0.0.0"
    Update=$(${Curl} -kLsXPUT -H"Authorization: sso-key ${Key}:${Secret}" \
    -H"Content-type: application/json" \
    https://api.godaddy.com/v1/domains/${Domain}/records/${Type}/${Name} \
    -d "[{\"data\":\"${PublicIP}\",\"ttl\":${TTL}}]" 2>/dev/null)
    
function gdapicall () {
    headers="Authorization: sso-key $key:$secret"

    response=$(curl -s -w "%{http_code}" -X PUT "https://api.godaddy.com/v1/domains/$domain/records/$type/$name" \
    -H "accept: application/json" \
    -H "Content-Type: application/json" \
    -H "${headers}" \
    -d "[ { \"data\": \"${cdns}\", \"ttl\": ${ttl} } ]")
    return ${response}
}

[ -d /var/cache/dyndns ] || mkdir -p /var/cache/dyndns

if [ -f "${cachefile}" ]; then
    # Verify that the file is a text file
    if [ $(file -i "${cachefile}" | cut -d' ' -f2) != "text/plain;" ]; then
        echo "Cache file seems to be wrong or corrupted." 1>&2
        exit 2
    fi
    
    if [ 16 -lt $(wc -c < $cachefile) ]; then
        echo "Cache file seems to be wrong or corrupted."  1>&2
        exit 2
    fi
    currentip=$(cat $cachefile)
fi

if [ "${currentip}" == "${cdns}" ]; then
    logger -p info -t DYNDNS "WAP IP not changed."
else
    logger -p info -t DYNDNS "WAP IP changed. New IP: ${cdns}. Updating GoDaddy"
    gdapicall
    if [ "${response}" == '200' ]; then
        echo "${cdns}" > ${cachefile}
        logger -p info -t DYNDNS "GoDaddy Update was successful"
    else
        logger -p info -t DYNDNS "GoDaddy Update failed. Response code was ${response}"
    fi
fi

exit 0
